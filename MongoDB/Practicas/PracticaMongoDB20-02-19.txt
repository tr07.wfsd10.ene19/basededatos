1.- insertar en la colección ciudades
{ "ciudad" : "Madrid", "habitantes" : 3233527, capital: “si”}
{ "ciudad" : "Barcelona", "habitantes" : 1620943 }
{"ciudad" : "Valencia", "habitantes" : 797028 }
{ "ciudad" : "Sevilla", "habitantes" : 702355 }
{"ciudad" : "Zaragoza", "habitantes" : 679624 }
2.- Obtener los tres primeros documentos completos, ordenados alfabéticamente.

    db.ciudades.find().sort({ciudad:1});

3.- Mostrar los documentos para que se vean ordenados por población. El  mayor el primero.

    db.ciudades.find().sort({habitantes:-1});

4.- Obtener las ciudades que empiecen por “M”

    db.ciudades.find(
        {ciudad: /^M/},
        {_id:0, habitantes:0}
    );

5.- Ordenar , a la vez, primero por nombre (desc) y luego por número de habitantes (asc)

    db.ciudades.find().sort({ciudad:-1,habitantes:1});

6.- Obtener aquellas ciudades que sean capital, y mostrarlo.

    db.ciudades.find(
        {capital: {$exists:true}},
        {ciudad:1,_id:0}
    );

7.- Mostrar el nombre de  las ciudades que superen un millón de habitantes

    db.ciudades.find(
        {habitantes: {$gt:1000000}}
    );

8.-¿Qué devolverá la consulta: db.ciudades.find({ciudad:{$in:['Avila','Zamora','Madrid']}})?

    Devuelve todos los documentos cuyo nombre de ciudad sea: Avila, Zamora, Madrid. En
    este caso solo devuelve Madrid, ya que las otras no existen en la coleccion.

9.- ¿Número de ciudades que componen la colección?

    5

10.- Ciudades que empiecen por “B” o acaben por “z”

    db.ciudades.find(
        {$or:[{ciudad:{$regex:/^B/}},{ciudad:{$regex:/Z$/}}]}
    );

    db.ciudades.find(
        {ciudad: {$in:[/^B/,/z$/]}}
    );

11.- Cuantas ciudades tienen menos de 800000 paises

    db.ciudades.find(
        { habitantes: {$lt:800000} }
    ).count();









