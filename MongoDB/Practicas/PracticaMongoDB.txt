¿Cuántos alumnos componen mi colección?

    db.alumnos.count();

Seleccionar aquellos documentos con una edad de 20 o de 15 años

    db.alumnos.find(
        {edad:{$in:[20,15]}}
        );

Seleccionar los alumnos cuyo apellido empiece por A,B,C, o D

    db.alumnos.find(
        { nombre: {$lte: "D"}}
    );

Seleccionar aquellos alumnos que tengan sean mayores de edad

    db.alumnos.find(
        { edad: {$gte: 18}}
    );

Seleccionar aquellos alumnos que tengan entre 20 y 30 años. Justificad la respuesta

    db.alumnos.find(
        {edad: {$gte: 20, $lte:30} }
    );

Renombrar la Colección a ClaseUpgrade

    db.alumnos.renameCollection("ClaseUpgrade");